import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChunhuischoolComponent } from './chunhuischool.component';

describe('ChunhuischoolComponent', () => {
  let component: ChunhuischoolComponent;
  let fixture: ComponentFixture<ChunhuischoolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChunhuischoolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChunhuischoolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
