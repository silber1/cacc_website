import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ArticleService } from "../../services/article.service"
import { FileService } from "../../services/file.service"
import { EventService } from "../../services/event.service" 
@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
  // info-chuck 
  infoChucks: any[] = [
    {
      title: "ABOUT US",
      article: "Chinese American Community Center (CACC, 德立華華美聯誼中心), \
                in Hockessin, Delaware, is a non-profit, non-sectarian,\
                not partisan and independent organization founded in 1982 by a group of interested Chinese Americans in the Greater Wilmington\
                area which encompasses Delaware, southeastern Pennsylvania, and southern New Jersey. CACC is an affiliate agency of the United Way of Delaware.",
      imgSource: "../../../assets/AboutUs/aboutus3.jpg"
    },
    {
      title: "OUR MISSION",
      article: "The primary mission of CACC is to promote the exchange and integration of Chinese and American cultures\
                by coordinating activities and events throughout the year and by providing a location for various community organizations and clubs to meet. \n \
                CACC is a resource center for its members and the people in the public who are interested in Chinese culture and language.\
                The mission of the CACC is to promote the exchange and integration of Chinese\
                and American cultures through the cultural and educational programs that we offer and through community service. \
                In this way, CACC is able to inspire understanding and appreciation of the Chinese heritage, \
                to encourage members to participate in community services and become an integral part of the mainstream in Delaware.",
      imgSource: "../../../assets/AboutUs/aboutus2.jpg"
    },
    {
      title: "OUR PROGRAMS AND ACTIVITIES",
      article: "CACC operates Montessori School and Child Care Center on weekdays. \
                It hosts two Chinese Schools on weekends which teach kids and teens Chinese language and culture during the school year. \
                CACC sponsors many regular club activities, workshops, classes, seminars, holidays celebrations, concerts, Chinese Culture Camp\
                and Chinese Festival. \n \
                The membership of CACC consists of individuals and families with diverse background and professions. \
                For further information about CACC, please contact Center Manager at 302-239-0432. \n \
                Check us out on Facebook: www.Facebook.com/caccDelaware \n \
                Chinese American Community Center (CACC) \n \
                1313 Little Baltimore Rd \n \
                Hockessin, DE 19707-9701 \n \
                (302) 239-0432 \n \
                United Way code: 9309 \n",
      imgSource: "../../../assets/AboutUs/aboutus4.jpg"
    },
    {
      title: "CACC BOARD OF DIRECTORS",
      article: "Period: April 2018 to March 2019 \n \
                \n \
                \n \
                Officers: \n \
                \n \
                Barbara Silber 蕭齊, Chair 主席 \n \
                Naxin Cai 蔡納新, Vice Chair 副主席 \n \
                Jun Sun 孙骏, Secretary 秘書 \n \
                Tiffany Zhan 詹雅, Treasurer 財務 \n \
                \n \
                Directors 理事: \n \
                \n \
                Ivan Sun 孫懿賢, Director \n \
                Fancia Tang 湯竹芬, Director \n \
                Jack Tsai 蔡維介, Director \n \
                Ping Xu 徐平, Director \n ", 
      imgSource: "../../../assets/AboutUs/aboutus1.jpg"
    }
  ];
  // event-card
  eventCards: any[] = [
    {
      eventName: "Moon Festival",
      month: "Nov",
      date: "1",
      time: "12pm",
      endTime: '',
      imgSource: "../../../assets/Event/22137197_1241017312671143_5219685022138012390_o.jpg",

    },
    {
      eventName: "CACC Summer Camp",
      month: "Nov",
      date: "1",
      time: "12pm",
      endTime: '',
      imgSource: "../../../assets/Event/36660313_1498203030285902_6030530053798887424_o.jpg",

    },
    {
      eventName: "Moon Festival",
      month: "Nov",
      date: "1",
      time: "12pm",
      endTime: '',
      imgSource: "../../../assets/Event/41990544_1603633246409546_3350110091863916544_n.jpg",

    },

  ];
  // carousel
  carouselImgSources: string[] = [
    "../../../assets/Home/img1-ouslsj0ic3zed8js1iez4ybap0dg23dabyy926d3v8.jpg",
    "../../../assets/Home/img2-ouslsj0ic3zed8js1iez4ybap0dg23dabyy926d3v8.jpg",
    "../../../assets/Home/img3-ouslsj0ic3zed8js1iez4ybap0dg23dabyy926d3v8.jpg",
    "../../../assets/Home/img4-ouslsj0ic3zed8js1iez4ybap0dg23dabyy926d3v8.jpg",
    "../../../assets/Home/img5-ouslsj0ic3zed8js1iez4ybap0dg23dabyy926d3v8.jpg",
    "../../../assets/Home/img6-ouslsj0ic3zed8js1iez4ybap0dg23dabyy926d3v8.jpg",
    "../../../assets/Home/img7-ouslsj0ic3zed8js1iez4ybap0dg23dabyy926d3v8.jpg"





  ];
  constructor(
    private router: Router,
    private _articleSvc: ArticleService,
    private _fileSvc : FileService,
    private _eventSvc : EventService
  ) { }
e
  ngOnInit(): void {
    this._articleSvc.getArticleByCategory("Home").subscribe(data => {
      console.log(`home page:`);
      console.log(data);
      if (data.length < 4){
        console.log("home page back end data for article about us is not enough");
      }else {
        // assign value
        this.infoChucks.forEach((each, index) => {
          each.title = data[index].title;
          each.article = this._articleSvc.getContent(data[index].content);
          each.imgSource = this._articleSvc.getImageSources(data[index].content)[0];
        });
        console.log(this.infoChucks);
      }
    });

    this._eventSvc.getAllEvents().subscribe(data => {
      console.log("home page latest event:");
      console.log(data);
      if (data.length <3){
        console.log("home page back end data for latest event is not enough");
      }else {
        this.eventCards.forEach((each, index) => {
          each.eventName = data[index].title;
          each.month = this._eventSvc.getMonth(data[index].date);
          each.date = data[index].date.split("-")[2];
          each.time = data[index].time;
          each.endTime = data[index].endTime;
          each.imgSource = this._eventSvc.getImageSources(data[index].content)[0];
        })
      }
    })
    this._fileSvc.getFilesByCategory("Home").subscribe(data => {
      console.log(`home page carousel: ${data}`);
    })
  }

  onEventCardClicked(){
    this.router.navigate(['/event']);
    window.location.hash = "";
    window.location.hash = "'main-events'";
  }
}
