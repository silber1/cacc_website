import { Component, OnInit } from '@angular/core';
import { ClubService } from '../../services/club.service';
import { ArticleService } from '../../services/article.service';
import { backEndUrl } from 'src/environments/environment';
import { FileService } from '../../services/file.service';
import { EventService } from '../../services/event.service';

@Component({
  selector: 'app-club-main-page',
  templateUrl: './club-main-page.component.html',
  styleUrls: ['./club-main-page.component.scss']
})

export class ClubMainPageComponent implements OnInit {

  constructor(
    private clubService: ClubService,
    private articleService: ArticleService,
    private fileService: FileService,
    private eventService: EventService,
  ) { }

  private clubUrl = `${backEndUrl}`;
  
  carouselImgSources: string[] = [
    "../../../assets/ClubMain/clubs1.jpg",
    "../../../assets/ClubMain/clubs2.jpg",
    "../../../assets/ClubMain/chorus2.jpg"
  ];

  infoChunk = {
      title: "About Clubs",
      article: "NoContent",
      imgSource: "../../../assets/ClubMain/clubs3.jpg"
  }

  events_r1: Array<Object> = [
    {
      date: new Date(),
      title: "Dancing Class",
      club: "Dancing Club",
      description: "Learning basic dancing skills."
    },
    {
      date: new Date(),
      title: "Writing Class",
      club: "Writing Club",
      description: "Learning basic dancing skills."
    },
    {
      date: new Date(),
      title: "Drawing Class",
      club: "Drawing Club",
      description: "Learning basic dancing skills."
    },
    {
      date: new Date(),
      title: "Test Class",
      club: "Test Club",
      description: "Learning basic dancing skills."
    }
  ]

  events_r2: Array<Object> = [
    {
      date: new Date(),
      title: "Sing Class",
      club: "SIng Club",
      description: "Learning basic sing skills."
    },
    {
      date: new Date(),
      title: "Test Class",
      club: "Dancing Club",
      description: "Learning basic dancing skills."
    },
    {
      date: new Date(),
      title: "Dancing Class",
      club: "Test Club",
      description: "Learning basic dancing skills."
    },
    {
      date: new Date(),
      title: "Dancing Class",
      club: "Test Club",
      description: "Learning basic dancing skills."
    }
  ]

  clubs: any;

  ngOnInit(): void {
    this.getAllClubs();
    this.getArticle();
    this.getFiles();
  }

  public navigateToSection(section: string) {
    window.location.hash = '';
    window.location.hash = section;
  }

  public counter(i: number) {
    return new Array(i)
  }

  getAllClubs(): void {
    this.clubService.getAllClubs().subscribe((data) => this.clubs = data);
  }

  getArticle(): void {
    this.articleService.getArticleByCategory("Clubs").subscribe((data) => {
      if (data.length < 1) {
        console.log("No About US information found");
      } else {
        this.infoChunk.title = data[0].title;
        this.infoChunk.article = this.articleService.getContent(data[0].content);
        this.infoChunk.imgSource = this.articleService.getImageSources(data[0].content)[0];
      }
    });
  }

  getFiles() {
    this.fileService.getFilesByCategory("Clubs").subscribe((data) => this.getImagePath(data.data));
  }

  //TODO: this function pushes path into the array. Remove all the default path before push new path.
  getImagePath(data) {
    let re = /apiuploads/gi; 
    data.forEach(element => {
      let tempPath = this.clubUrl + element.path;
      let path = tempPath.replace(re, "static");
      this.carouselImgSources.push(path);
    });
  }

  //TODO: Load events from backend and display on the web
}
