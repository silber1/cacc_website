import { Component, OnInit } from '@angular/core';
import * as $ from '../../../../node_modules/jquery';
import { AuthService } from '../../services/auth.service';
@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  inputUserName: string = "";
  inputPassword: string = "";

  constructor(
    private _authSvc: AuthService
  ) { }

  ngOnInit(): void {
    $("#submitBtn").click(() => {
      this.inputUserName = $("#login").val();
      this.inputPassword = $("#password").val();
      
      this._authSvc.login(this.inputUserName, this.inputPassword).subscribe((data) => {
        //console.log(data);
        if (data.error){
          alert(data.error.error);

        }

      })
    });
  }

}
