import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-summer-camp',
  templateUrl: './summer-camp.component.html',
  styleUrls: ['./summer-camp.component.scss']
})
export class SummerCampComponent implements OnInit {
  // info-chuck 
  infoChucks: any[] = [
    {
      title: "CACC Summer Camp 1",
      article: "Brrr, it's cold outside! Perfect weather to start dreaming about CACC summertime fun! http://www.caccdelaware.org/summer-camp.html \
                Visit the CACC Summer Camp Fun Squad tomorrow at Community Camp Fair at Tower Hill De",
      imgSource: "../../../assets/SummerCamp/summercampevent1.jpg",
    },
    {
      title: "CACC Summer Camp 2",
      article: "Catch a CACC 2018 Summer Camp Early Bird discount! Hurry! Early Bird Registration ends on March 31, 2018. \
                Summer camp fun is just around the corner! More Weeks = More Fun! \
                www.caccdelaware.org/summer-camp.html", 
      imgSource: "../../../assets/SummerCamp/summercampevent2.jpg",
    }
  ];
  // event-card
  eventCards: any[] = [
    {
      eventName: "CACC Summer Camp 1",
      month: "Oct",
      date: "19",
      time: "12pm",
      imgSource: "../../../assets/SummerCamp/summercampevent1.jpg",

    },
    {
      eventName: " CACC Summer Camp 2 ",
      month: "Nov",
      date: "5",
      time: "12pm",
      imgSource: "../../../assets/SummerCamp/summercampevent2.jpg",

    },
    {
      eventName: "CACC Summer Camp3 ",
      month: "Nov",
      date: "10",
      time: "12pm",
      imgSource: "../../../assets/SummerCamp/summercampevent3.jpg",
    },

  ];
  // carousel
  carouselImgSources: string[] = [
    "../../../assets/SummerCamp/summercamp.jpg",
    //"../../../assets/izone bloomiz.jpg",
    //"../../../assets/izone coloriz.jpg"
  ];
  constructor() { }

  ngOnInit(): void {
  }

}
