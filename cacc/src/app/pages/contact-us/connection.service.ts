import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, tap } from 'rxjs/operators';
import {of} from 'rxjs';
import { backEndUrl } from 'src/environments/environment';

@Injectable({
providedIn: 'root'
})
export class ConnectionService {
url: string = `${backEndUrl}/mailService`;
constructor(private http: HttpClient) { }

sendMessage(messageContent: any) {
  return this.http.post(this.url + "/send", //the address of the endpoint to be hit by the front layer
  messageContent, //the data that the front has to send to the address from the first parameter
  { headers: new HttpHeaders({ 'Content-Type': 'application/json' })}
  ).pipe(
    catchError(error => of (error)),
    map((header:any) => {
      return header;
    })
  )
  //optional: the Http header with the defined Content-Type parameter
}
}