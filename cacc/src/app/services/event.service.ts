import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import {of} from 'rxjs';
import { backEndUrl } from 'src/environments/environment';
import { AdminService } from './admin.service';

@Injectable({
  providedIn: 'root'
})
export class EventService {
  url:any = `${backEndUrl}/events`;//"http://localhost:3000/api/events";

  monthMap: string[] = ['Jan.', 'Feb.', 'Mar.', 'Apr.', "May.", "Jun.", 'Jul.', 'Aug.', 'Sep.', 'Oct.','Nov.','Dec.']
  private headers = new HttpHeaders({
    "Content-Type": "application/json"
  });

  constructor(
    private _http: HttpClient,
    private _adminSvc: AdminService
    ) { }

  /* write service to upload or download event */
  /* GET API*/ 
  public getAllEvents(

  ){
    return this._http
    .get<any>(
      this.url + `/`,
      {headers : this.headers}
    ).pipe(
      catchError(error => of (error)),
      map((header:any) => {
        console.log(header);
        return header;
      })
    )
  }
  public getEventsByCategory(
    category: string
  ){
    return this._http
    .get<any>(
      this.url + `/category/${category}`,
      {headers : this.headers}
    ).pipe(
      catchError(error => of (error)),
      map((header:any) => {
        console.log(header);
        return header;
      })
    );
  }

  public getEventByTitle(
    title: string
  ){
    return this._http
    .get<any>(
      this.url + `/title/${title}`,
      {headers : this.headers}
    ).pipe(
      catchError(error => of (error)),
      map((header:any) => {
        console.log(header);
        return header;
      })
    );
  }

  public searchEventByTitle(
    title: string
  ){
    return this._http
    .get<any>(
      this.url + `/search/${title}`,
      {headers : this.headers}
    ).pipe(
      catchError(error => of (error)),
      map((header:any) => {
        console.log(header);
        return header;
      })
    )
  }


  /* POST API*/ 
  public addEvent(
    title: string,
    content: string,
    category: string,
    date: string,
    address: string,
    time: string,
    endTime: string
  ){
    const headersWithAuth = new HttpHeaders({
      "Authorization": `Bearer ${this._adminSvc.getAccessToken()}`,
      "Content-Type": "application/json",
    });
    return this._http
    .post<any>(
      this.url + `/createEvent`,
      {
        content: content,
        title: title,
        category: category,
        date: date,
        address: address,
        time: time,
        endTime: endTime,
        role: this._adminSvc.getCurAdminRole()
      },
      {headers : headersWithAuth}
    ).pipe(
      catchError(error => of (error)),
      map((header:any) => {
        console.log(header);
        return header;
      })
    );
  }

  /*DELETE API*/
  public deleteEventById(
    id: any
  ){
    const headersWithAuth = new HttpHeaders({
      "Authorization": `Bearer ${this._adminSvc.getAccessToken()}`,
      "Content-Type": "application/json",
    });
    return this._http
    .delete<any>(
      this.url + `/delete/${id}`,
      {headers : headersWithAuth}
    ).pipe(
      catchError(error => of (error)),
      map((header:any) => {
        console.log(header);
        return header;
      })
    );
  }

  /*PUT API */
  public editEventById(
    id: any,
    title: string,
    content: string,
    category: string,
    date: string,
    address: string,
    time: string,
    endTime: string
  ){
    const headersWithAuth = new HttpHeaders({
      "Authorization": `Bearer ${this._adminSvc.getAccessToken()}`,
      "Content-Type": "application/json",
    });
    return this._http
    .put<any>(
      this.url + `/edit/${id}`,
      {
        content: content,
        title: title,
        category: category,
        date: date,
        address: address,
        time: time,
        endTime: endTime,
        role: this._adminSvc.getCurAdminRole()
      },
      {headers : headersWithAuth}
    ).pipe(
      catchError(error => of (error)),
      map((header:any)=> {
        console.log(header);
        return header;
      })
    )
  }

  /* helper function */
  getImageSources(str: string){
    //console.log(`incomming string: ${str}`);
    let imgsrc = [];
    let result = str.split(/src="/gi);
    var src = "";
    if (result.length >= 2){
      src = result[1].split(/">/gi)[0];
    }
    imgsrc.push(src);
    //console.log(imgsrc);
    return imgsrc;
  }
  
  getContent(str: string){
    let final = "";
    const secrete:string  = "##mysecrtejoin##";
    //console.log(`incomming string: ${str}`);
    let result = str.split(`<figure`).join(secrete);
    result = result.split('</figure>').join(secrete);
    result.split(secrete).forEach((each, index) => {
        if (index != 1){
          //console.log(each);
          final += each;
        }

    })

    return final;
  }

  getMonth(date: string){
    let month: number = +date.split('-')[1];
    //console.log("here:" + month);
    if (month > 0 && month <= this.monthMap.length){
      return this.monthMap[month-1];
    }

  }

}
