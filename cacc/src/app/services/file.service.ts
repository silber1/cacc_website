import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import {of} from 'rxjs';
import { backEndUrl } from 'src/environments/environment';
import { AdminService } from './admin.service';

@Injectable({
  providedIn: 'root'
})
export class FileService {
  url:any = `${backEndUrl}/files`;//'http://localhost:3000/api/files';
  private headers = new HttpHeaders({
    "Content-Type": "application/json"
  });

  constructor(
    private _http: HttpClient,
    private _adminSvc: AdminService
    ) { }

  /*write service to upload or download img or file*/

  /*GET API*/
  public getFilesByCategory(
    category:string
  ){
    return this._http
    .get<any>(
      this.url + `/${category}`,
      {headers: this.headers}
    ).pipe(
      catchError(error => of (error)),
      map((header:any) => {
        console.log(header);
        return header;
      })
    );
  }

  public getFileByName(
    name :string
  ){
    return this._http
    .get<any>(
      this.url + `/name/${name}`,
      {headers: this.headers}
    ).pipe(
      catchError(error => of (error)),
      map((header:any) => {
        console.log(header);
        return header;
      })
    );
  }

  /*POST API*/
  public uploadFileBase64(
    name: string,
    category: string,
    base64: string
  ){
    const headersWithAuth = new HttpHeaders({
      "Authorization": `Bearer ${this._adminSvc.getAccessToken()}`,
      "Content-Type": "application/json",
    });
    return this._http
    .post<any>(
      this.url + `/uploadFileBase64`,
      {
        name: name,
        category: category,
        base64: base64
      },
      
      {headers: headersWithAuth}
    ).pipe(
      catchError(error => of (error)),
      map((header: any) => {
        console.log(header);
        return header;
      })
    )
  }

  /* DELETE API */
  public deleteFileById(
    id:any
  ){
    const headersWithAuth = new HttpHeaders({
      "Authorization": `Bearer ${this._adminSvc.getAccessToken()}`,
      "Content-Type": "application/json",
    });
    return this._http
    .delete<any>(
      this.url + `/delete/${id}`,
      {headers: headersWithAuth}
    ).pipe(
      catchError(error => of (error)),
      map((header: any) => {
        console.log(header);
        return header;
      })
    );
  }

  
}
