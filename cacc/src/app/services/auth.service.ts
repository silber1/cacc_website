import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import {of} from 'rxjs';
import { backEndUrl } from 'src/environments/environment';
import { Router } from '@angular/router';
import { AdminService } from './admin.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  url:any = `${backEndUrl}/auth`;

  private headers = new HttpHeaders({
    "Content-Type": "application/json"
  });
  
  constructor(
    private router: Router,
    private _http: HttpClient,
    private _adminSvc: AdminService

  ) { }
  
  public register(
    email: string,
    password: string,
    confirmPassword: string,
    role: string
  ){
    const headersWithAuth = new HttpHeaders({
      "Authorization": `Bearer ${this._adminSvc.getAccessToken()}`,
      "Content-Type": "application/json",
    });
    return this._http
    .post<any>(
      this.url + '/register',
      {
        email: email,
        password: password,
        confirm_password: confirmPassword,
        role: role,
        curAdminRole: this._adminSvc.getCurAdminRole()
      },
      {headers: headersWithAuth}
    ).pipe(
      catchError(error => of (error)),
      map((admin: any) => {
        return admin;
      })
    )
  }

  public login(
    email: string,
    password: string
  ){
    return this._http
      .post<any>(
        this.url + '/login',
        {
          email: email,
          password: password
        },
        {headers: this.headers}
      ).pipe(
        catchError(error=> of (error)),
        map((admin: any) => {
          //console.log(admin);
          if (admin.error){
            this._adminSvc.setAdmin(null);
          }else {
          // use admin serivice
            this._adminSvc.setAdmin(admin);
          }
          // navigate to editor page
          this.router.navigate(['editor']);
          return admin;
        })
      );
  }

  public logout(){
    this._adminSvc.removeAdmin();
    this.router.navigate([''])
  }
}
