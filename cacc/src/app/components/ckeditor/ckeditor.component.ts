import { Component, OnInit, Input } from '@angular/core';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import * as $ from '../../../../node_modules/jquery';
import * as CustomEditor from '../../../assets/ckeditor.js';
import { ChangeEvent } from '@ckeditor/ckeditor5-angular/ckeditor.component';
import { EditorPageComponent } from '../../pages/editor-page/editor-page.component';
import { ArticleService } from "../../services/article.service"
import { FileService } from "../../services/file.service"
import { EventService } from '../../services/event.service';
import { ClubService } from '../../services/club.service';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'app-ckeditor',
  templateUrl: './ckeditor.component.html',
  styleUrls: ['./ckeditor.component.scss']
})
export class CkeditorComponent implements OnInit {
  public Editor = CustomEditor;
  @Input() curAdmin: any;

  info:string = "11111";

  authrizedClubs : string[];
  // selection option
  elementSelection : string[] = [
    "Carousel Image",
    "Article",
    "Event"
  ];
  pageSelection : string[] = [];
  editOptionSelection : string[] = [];
  idOrTitleSelection : string[] = [];

  elementSeletor : string;
  pageSelector : string;
  editOptionSelector : string;
  idOrTitleSelecotr : string;

  renderNgIf : boolean = false;
  renderInputGroup : boolean = false;
  renderImageEditGroup : boolean = false;
  renderAddingNewImage : boolean = false;
  renderEventInput : boolean = false;

  imageChangedEvent: any = '';
  croppedImage: any = '';

  // infoGroup
  event_id: any;
  title: string;
  category: string;
  date: string;
  address: string;
  content: string;
  time: string;
  endTime: string;

  // image edit group
  fileName: string;
  imgEditurl: string;
  imgId: any;

  constructor(
    private _articleSvc:ArticleService,
    private _fileSvc:FileService,
    private _eventSvc:EventService,
    private _clubSvc:ClubService
  ) { }

  ngOnInit(): void { 
    this.onElementSelectionChanged();
    this.onPageSelectionChanged();
    this.onEditOptionSelectionChanged();

    this.getAutherizedClub();
  }

  /* function for selection */
  onElementSelectionChanged(){
    $("#elementSelection").change( () => {
      this.renderInputGroup = false;
      this.renderImageEditGroup = false;
      this.idOrTitleSelection = [];
      let curVal = $("#elementSelection").val();
      // set pageSelection
      if (curVal == "Carousel Image"){
        this.pageSelection = [
        "Home",
        "Clubs",
        "Events",
        "Summer Camp",
      ];
      }else if (curVal == "Article"){
        this.pageSelection = [
          "Home",
          "Clubs",
          "CACC MONTESSORI SCHOOL",
          "CHINESE SCHOOL OF DELAWARE",
          "CHUN-HUI CHINESE SCHOOL",
          "Summer Camp"
        ];
        
      }else if (curVal == "Event"){
        this.pageSelection = [
          "Summer Camp"
        ];
      }
      if (this.curAdmin.role != "admin") this.pageSelection = [];
      this.pageSelection = this.pageSelection.concat(this.authrizedClubs);
    });
  }

  onPageSelectionChanged(){
    $("#pageSelection").change(() => {
      this.renderInputGroup = false;
      this.renderImageEditGroup = false;
      // set editOptionSelection
      this.editOptionSelection = ["Add","Edit","Delete"];
    });
  }

  onEditOptionSelectionChanged(){
    $("#editOptionSelection").change(()=> {
      this.renderInputGroup = false;
      this.renderImageEditGroup = false;
     let curVal = $("#editOptionSelection").val();
     if (curVal == "Add"){
      this.renderNgIf = false;
     }else{
       // set idOrTitleSelection
      this.requireService($("#elementSelection").val());
      
      // render idOrTitleSelection
      this.renderNgIf = true;
     }

     

    });
  }

  /* function for editor */
  onSelect(){
    this.info = "default content";
    this.elementSeletor = $("#elementSelection").val();
    this.pageSelector = $("#pageSelection").val();
    this.editOptionSelector = $("#editOptionSelection").val();
    if (this.renderNgIf){
      this.idOrTitleSelecotr = $("#idOrTitleSelection").val();
      if (!this.idOrTitleSelecotr) {alert("please select title or id"); return;}
    }else {
      this.idOrTitleSelecotr = null;
    }
    
    // check input
    if (!this.elementSeletor) {alert("please select element"); return;}
    if (!this.pageSelector) {alert("please select page"); return;}
    if (!this.editOptionSelector) {alert("please select edit option"); return;}
    // render Input/ImageEdit Group
    if(this.elementSeletor == "Carousel Image"){
      this.renderImageEditGroup = true;
      // render to add new Image OR editing existing image
      this.renderAddingNewImage = this.editOptionSelector == "Add";
    }else if (this.elementSeletor == "Article"){
      this.renderInputGroup = true;
      this.renderEventInput = false; 
    }else if (this.elementSeletor == "Event"){
      this.renderInputGroup = true;
      this.renderEventInput = true;
    }
    alert(`OPTION SELECTION: ${this.elementSeletor} -> ${this.pageSelector} -> ${this.editOptionSelector} -> ${this.idOrTitleSelecotr}`);
    
    // call service based on edit option
    switch (this.editOptionSelector){
      case "Add":{
        console.log("select Add");
        // nothing need to do currently
        break;
      }
      case "Edit":{
        console.log("select Edit");
        if (this.elementSeletor == "Article"){
          this._articleSvc.getArticleByTitle(this.idOrTitleSelecotr).subscribe(data => {
            //console.log(`get article: ${data}`);
            const myData = data[0];
            //this.getImageSources(data[0].content);
            //this.getContent(data[0].content);
            this.readInfoGroup(myData.id, myData.title, myData.category, myData.date, myData.address, myData.content, myData.time, myData.endTime);
          });
        }else if (this.elementSeletor == "Event"){
          console.log("event");
          this._eventSvc.getEventByTitle(this.idOrTitleSelecotr).subscribe(data => {
            const myData = data[0];
            this.readInfoGroup(myData.event_id, myData.title, myData.category, myData.date, myData.address, myData.content, myData.time, myData.endTime);
          })
        }else if (this.elementSeletor == "Carousel Image"){
          this._fileSvc.getFileByName(this.idOrTitleSelecotr).subscribe(data => {
            this.imgEditurl = data.data[0].path;
          });
        }
        break;
      }
      case "Delete":{
        console.log("select Delete");
        if (this.elementSeletor == "Article"){
          this._articleSvc.getArticleByTitle(this.idOrTitleSelecotr).subscribe(data => {
            //console.log(`get article: ${data}`);
            const myData = data[0];
            this.readInfoGroup(myData.id, myData.title, myData.category, myData.date, myData.address, myData.content, myData.time, myData.endTime);
          });
        }else if (this.elementSeletor == "Event"){
          console.log("event");
          this._eventSvc.getEventByTitle(this.idOrTitleSelecotr).subscribe(data => {
            const myData = data[0];
            this.readInfoGroup(myData.event_id, myData.title, myData.category, myData.date, myData.address, myData.content, myData.time, myData.endTime);
          });
        }else if (this.elementSeletor == "Carousel Image"){
          this._fileSvc.getFileByName(this.idOrTitleSelecotr).subscribe(data => {
            this.imgEditurl = data.data[0].path;
            this.imgId = data.data[0].id;
          });
        }
        break;
      }
      default:{
        console.log("WARNNING: HERE IS THE DEFAULT");
      }
    }
  }

  onSave(){
    if (!this.renderInputGroup){
      alert("please select the element first");
      return;
    }
    var confirmation = confirm(`CONFIRM! \n Do you want to ${this.editOptionSelector} ${this.idOrTitleSelecotr == null ? this.elementSeletor : this.idOrTitleSelecotr} in ${this.pageSelector}`);
    if (confirmation){
      this.getInfoGroup();
      if (this.elementSeletor == "Article")
        this.writeInfoGroup();
      else if (this.elementSeletor == "Event")
        this.writeInfoGroupToEvent();
    }else {
      //console.log("No")
    }
    
    // this._articleSvc.addArticle("title2", "hereis the content", "about us", "2020/11/10", "ud").subscribe(date=> {
    //   console.log(`add article ${date}`);
    // });
    // this.renderInputGroup = false;
    // this.renderImageEditGroup = false;
  }

  onChange({editor}: ChangeEvent){
    this.info = editor.data();
  }

  onUploadImage(){
    if (this.editOptionSelector == "Add" || this.editOptionSelector == "Edit"){
      console.log("add or edit image");
      this._fileSvc.uploadFileBase64(this.fileName, this.pageSelector, this.croppedImage).subscribe((data) => {
        console.log(data);
      });
    }else if (this.editOptionSelector == "Delete"){
      console.log("delete image");
      this._fileSvc.deleteFileById(this.imgId).subscribe(data => {
        console.log(data);
        console.log(`deleted id:${this.imgId} name: ${this.idOrTitleSelecotr}`);
      })
    }

    this.renderInputGroup = false;
    this.renderImageEditGroup = false;
  }

  requireService(svcType : string){
    if (svcType == "Carousel Image"){
      //console.log("Carousel image service have not implement");
      this._fileSvc.getFilesByCategory($("#pageSelection").val()).subscribe(data => {
        this.idOrTitleSelection = [];
        data.data.forEach((each) => {
          this.idOrTitleSelection.push(each.name);
        });
      })
    }
    else if (svcType == "Article"){
      this._articleSvc.getArticleByCategory($("#pageSelection").val()).subscribe(data=> {
        this.idOrTitleSelection = [];
        console.log(data);
        data.forEach((each) => {
          this.idOrTitleSelection.push(each.title);
        });
      });
    }else if (svcType == "Event"){
      console.log("event");
      this._eventSvc.getEventsByCategory($("#pageSelection").val()).subscribe(data => {
        this.idOrTitleSelection = [];
        data.forEach((each) => {
          this.idOrTitleSelection.push(each.title);
        });
      });
    }
  }

  getInfoGroup(){
    this.title = $('#title').val();
    this.category = $('#category').val();
    this.date = $('#date').val();
    this.address = $('#address').val();
    this.time = $('#time').val();
    this.endTime = $('#timeEnd').val();
    this.content = this.info;
    console.log(this.time);
  }

  readInfoGroup(id: any, title: string, category:string, date:string, address:string, content:string, time: string, endTime: string){
    this.event_id = id;
    this.title = title;
    this.category = category;
    this.date = date;
    this.address = address;
    this.info = content;
    this.content = this.info;
    this.time = time;
    this.endTime = endTime;

    $('#timeEnd').val(this.endTime);
    $('#time').val(this.time);
    $('#title').val(this.title);
    $('#category').val(this.category);
    console.log(this.category);
    // convert yyyy/mm/dd to yyyy-mm-dd
    if (this.elementSeletor == "Event"){
      this.date = this.date.split("/").join("-");
      $('#date').val(this.date);
      $('#address').val(this.address);
    }else if (this.elementSeletor == "Article"){
      
    }
  }

  writeInfoGroup(){
    switch(this.editOptionSelector){
      case "Add": {
        this._articleSvc.addArticle(this.title, this.content, this.category).subscribe(data => {
          this.handleSubscribe(data, this.editOptionSelector, ()=> {});
        });
        console.log(`ADD: title: ${this.title}; category: ${this.category}; date: ${this.date}\
          address: ${this.address}; content: ${this.content}`);
        break;
      }
      case "Edit": {
        console.log("edit");
        this._articleSvc.editArticleById(this.event_id, this.title, this.content, this.category).subscribe(data => {
          this.handleSubscribe(data, this.editOptionSelector, ()=> {});
        })
        console.log(`EDIT id:${this.event_id}: title: ${this.title}; category: ${this.category}; date: ${this.date}\
          address: ${this.address}; content: ${this.content}`);
        break;
      }
      case "Delete": {
        console.log("delete");
        this._articleSvc.deleteArticleById(this.event_id).subscribe(data => {
          this.handleSubscribe(data, this.editOptionSelector, ()=> {});
        });
        console.log(`DELETE id:${this.event_id}: title: ${this.title}; category: ${this.category}; date: ${this.date}\
          address: ${this.address}; content: ${this.content}`);
        break;
      }
      default:{
        console.log("WARNNING: here is the default");
      }
    }


    // update idOrTitleSelection
    this.requireService(this.elementSeletor);
  }

  writeInfoGroupToEvent(){
    switch(this.editOptionSelector){
      case "Add": {
        console.log("add");
        this._eventSvc.addEvent(this.title, this.content, this.category, this.date, this.address, this.time, this.endTime).subscribe(data => {
          this.handleSubscribe(data, this.editOptionSelector, ()=> {});
        });
        console.log(`ADD: title: ${this.title}; category: ${this.category}; date: ${this.date}\
          address: ${this.address}; content: ${this.content}`);
        break;
      }
      case "Edit": {
        console.log("edit");
        this._eventSvc.editEventById(this.event_id, this.title, this.content, this.category, this.date, this.address, this.time, this.endTime).subscribe(data => {
          this.handleSubscribe(data, this.editOptionSelector, ()=> {});
        })
        console.log(`EDIT id:${this.event_id}: title: ${this.title}; category: ${this.category}; date: ${this.date}\
          address: ${this.address}; content: ${this.content}`);
        break;
      }
      case "Delete": {
        console.log("delete");
        this._eventSvc.deleteEventById(this.event_id).subscribe(data => {
          this.handleSubscribe(data, this.editOptionSelector, ()=> {});
        });
        console.log(`DELETE id:${this.event_id}: title: ${this.title}; category: ${this.category}; date: ${this.date}\
          address: ${this.address}; content: ${this.content}`);
        break;
      }
      default:{
        console.log("WARNNING: here is the default");
      }

    }


    // update idOrTitleSelection
    this.requireService(this.elementSeletor);
  }

  getAutherizedClub(){
    this.authrizedClubs= [];
    if (!this.curAdmin){
      return;
    }
    if (this.curAdmin.role == 'admin'){
      this._clubSvc.getAllClubs().subscribe(data => {
        //console.log(data);
        // check with the current user roler
        data.forEach((each) => {
          this.authrizedClubs.push(each.engname); //+ " " + each.chiname );
        })
      });
    }else {
      this.authrizedClubs.push(this.curAdmin.role);
    }
  }

  handleSubscribe(data: any, editOption: string, callback : () => void){
    if (!data){
      throw new Error("data is undefined");
    }

    if (data.error){
      alert(data.error.error);
    }else {
      console.log(`${editOption}: ${data}` );
      callback();
      this.renderInputGroup = false;
      this.renderImageEditGroup = false;
      this.requireService(this.elementSeletor);
    }
  }
  /* function for image cropping*/

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
    this.fileName = $('#imgUpload').val().split('\\').pop();
    console.log(this.fileName);
  }
  imageCropped(event: ImageCroppedEvent) {
      this.croppedImage = event.base64;
      //console.log(this.croppedImage);
  }
  imageLoaded() {
      // show cropper
  }
  cropperReady() {
      // cropper ready
  }
  loadImageFailed() {
      // show message
  }

}
