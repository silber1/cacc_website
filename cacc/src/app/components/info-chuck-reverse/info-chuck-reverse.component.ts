import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-info-chuck-reverse',
  templateUrl: './info-chuck-reverse.component.html',
  styleUrls: ['./info-chuck-reverse.component.scss']
})
export class InfoChuckReverseComponent implements OnInit {
  @Input() title: string;
  @Input() article: string;
  @Input() imgSource: string;
  constructor() { }

  ngOnInit(): void {
  }

}
