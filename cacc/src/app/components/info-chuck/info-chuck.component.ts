import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-info-chuck',
  templateUrl: './info-chuck.component.html',
  styleUrls: ['./info-chuck.component.scss']
})
export class InfoChuckComponent implements OnInit {
  // title: string = "Sub Title";
  // article: string = "Breaking it down, here’s how it works:Containers provide a means to center and horizontally pad your site’s contents. Use .container for aresponsive pixel width or .container-fluid for width: 100% across all viewport and device sizes. Rows are wrappers for columns. ";
  @Input() title: string;
  @Input() article: string;
  @Input() imgSource: string;
  @Input('master') masterName: string; // tslint:disable-line: no-input-rename

  constructor() { }

  ngOnInit(): void {
    
  }

}
