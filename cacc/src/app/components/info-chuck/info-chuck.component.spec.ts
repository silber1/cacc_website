import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoChuckComponent } from './info-chuck.component';

describe('InfoChuckComponent', () => {
  let component: InfoChuckComponent;
  let fixture: ComponentFixture<InfoChuckComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoChuckComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoChuckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
